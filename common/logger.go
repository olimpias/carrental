package common

import (
	"os"

	"io"

	"github.com/sirupsen/logrus"
)

const (
	logFileName = "logger.log"
)

var logging *logrus.Logger

func init() {
	logging = logrus.New()
	f, err := os.OpenFile(logFileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	} else {
		multiWriter := io.MultiWriter(f, os.Stdout)
		logging.Out = multiWriter
	}
}

// Log exports the logging
func Log() *logrus.Logger {
	return logging
}
