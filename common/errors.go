package common

import "errors"

var (
	ErrItemAlreadyExists = errors.New("item has already added")
	ErrItemNotFound      = errors.New("item is not found")
)
