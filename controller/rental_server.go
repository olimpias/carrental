package controller

import (
	"carrental/common"
	"carrental/service"
	"encoding/json"
	"net/http"

	"carrental/model"

	"github.com/gorilla/mux"
)

const (
	userId = "userId"
	carId  = "carId"
)

type CarRentalServer struct {
	Router      *mux.Router
	address     string
	carService  service.ICarService
	userService service.IUserService
	rentService service.IRentalService
}

func NewCarRentalServer(address string) *CarRentalServer {
	return NewCarRentalServerWithServices(address, service.CarService, service.UserService, service.RentalService)
}

func NewCarRentalServerWithServices(address string, carService service.ICarService, userService service.IUserService,
	rentService service.IRentalService) *CarRentalServer {
	return &CarRentalServer{Router: mux.NewRouter(), address: address, carService: carService,
		rentService: rentService, userService: userService}
}

func (cr *CarRentalServer) Serve() {
	go cr.serve()
}

func (cr *CarRentalServer) serve() {
	common.Log().Infof("Http server is going to serve on %s", cr.address)
	if err := http.ListenAndServe(cr.address, cr.Router); err != nil {
		common.Log().Fatal(err)
	}
}

func (cr *CarRentalServer) createCar(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	defer r.Body.Close()
	var car model.Car
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&car); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}
	if err := cr.carService.SaveCar(&car); err != nil {
		respondWithError(w, http.StatusConflict, err.Error())
	}
	respondWithJSON(w, http.StatusOK, nil)
}

func (cr *CarRentalServer) listCars(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	cars := cr.carService.ListCars()
	respondWithJSON(w, http.StatusOK, cars)
}

func (cr *CarRentalServer) createUser(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	defer r.Body.Close()
	var user model.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}
	if err := cr.userService.SaveUser(&user); err != nil {
		respondWithError(w, http.StatusConflict, err.Error())
	}
	respondWithJSON(w, http.StatusOK, nil)
}

func (cr *CarRentalServer) listUsers(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	users := cr.userService.ListUsers()
	respondWithJSON(w, http.StatusOK, users)
}

func (cr *CarRentalServer) rentCar(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	vars := mux.Vars(r)
	carId := vars[carId]
	userId := vars[userId]
	common.Log().Infof("Rent Car request with CarId : %s, UserId: %s", carId, userId)
	if err := cr.rentService.RentCar(userId, carId); err != nil {
		code := http.StatusConflict
		if err == common.ErrItemNotFound {
			code = http.StatusNotFound
		}
		respondWithError(w, code, err.Error())
		return
	}
	respondWithJSON(w, http.StatusOK, nil)
}

func (cr *CarRentalServer) rentedCarInfo(w http.ResponseWriter, r *http.Request) {
	common.Log().Infof("Request received with URL: %v", r.URL)
	rents := cr.rentService.ListRents()
	respondWithJSON(w, http.StatusOK, rents)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	var response []byte
	if payload != nil {
		res, err := json.Marshal(payload)
		if err != nil {
			response = []byte(err.Error())
			code = http.StatusInternalServerError
			goto headerWrite
		}
		w.Header().Set("Content-Type", "application/json")
		response = res
	}
headerWrite:
	w.WriteHeader(code)
	w.Write(response)
}
