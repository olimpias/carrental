package controller

import (
	"carrental/common"
	"net/http"
)

type PprofServer struct {
	mux     *http.ServeMux
	address string
}

func NewPprofServer(address string) *PprofServer {
	return &PprofServer{mux: http.NewServeMux(), address: address}
}

func (ph *PprofServer) Serve() {
	go ph.serve()
}

func (ph *PprofServer) serve() {
	common.Log().Infof("Pprof http server is going to serve on %s", ph.address)
	if err := http.ListenAndServe(ph.address, ph.mux); err != nil {
		common.Log().Error(err)
	}
}
