package controller

import (
	"net/http"
	"net/http/pprof"
)

const (
	UserPath               = "/user"
	CarPath                = "/car"
	RentPath               = "/rent"
	RentPathWithCarAndUser = "/rent/user/{userId}/car/{carId}"
)

func InitializeRentalRouter(handler *CarRentalServer) {
	handler.Router.Methods(http.MethodPost).PathPrefix(UserPath).HandlerFunc(handler.createUser)
	handler.Router.Methods(http.MethodGet).PathPrefix(UserPath).HandlerFunc(handler.listUsers)
	handler.Router.Methods(http.MethodPost).PathPrefix(CarPath).HandlerFunc(handler.createCar)
	handler.Router.Methods(http.MethodGet).PathPrefix(CarPath).HandlerFunc(handler.listCars)
	handler.Router.Methods(http.MethodPost).Path(RentPathWithCarAndUser).HandlerFunc(handler.rentCar)
	handler.Router.Methods(http.MethodGet).Path(RentPath).HandlerFunc(handler.rentedCarInfo)
}

func InitializePprofRouter(handler *PprofServer) {
	handler.mux.HandleFunc("/debug/pprof/", pprof.Index)
	handler.mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	handler.mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	handler.mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	handler.mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
}
