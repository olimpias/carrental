package controller

import (
	"bytes"
	"carrental/model"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCreateCar(t *testing.T) {
	tests := []struct {
		expectedStatus int
		inputBody      string
	}{
		{expectedStatus: http.StatusOK, inputBody: "{\"id\":\"494949494\",\"name\":\"cem\",\"surname\":\"turker\",\"age\":14}"},
		{expectedStatus: http.StatusBadRequest, inputBody: "asdada"},
	}

	for _, test := range tests {
		byteBuffer := bytes.NewBufferString(test.inputBody)
		req, err := http.NewRequest(http.MethodPost, UserPath, byteBuffer)
		if err != nil {
			t.Fatal(err)
		}
		carRentalHandler := NewCarRentalServer("")
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(carRentalHandler.createUser)
		handler.ServeHTTP(rr, req)
		if status := rr.Code; status != test.expectedStatus {
			t.Errorf("Status received %d, but expected status %d", status, test.expectedStatus)
		}
	}
}

type MockUserService struct {
	users []*model.User
}

func (m *MockUserService) CreateMockUsers() {
	m.users = []*model.User{
		model.NewUser("123123", "cem", "turker", 15),
		model.NewUser("1231241", "test", "test", 20),
	}
}

func (m *MockUserService) SaveUser(user *model.User) error {
	return nil
}
func (m *MockUserService) FindUser(id string) (*model.User, error) {
	return nil, nil
}
func (m *MockUserService) ListUsers() []*model.User {
	return m.users
}

func TestListUsers(t *testing.T) {
	req, err := http.NewRequest(http.MethodGet, UserPath, nil)
	if err != nil {
		t.Fatal(err)
	}
	mockService := MockUserService{}
	mockService.CreateMockUsers()
	carRentalHandler := NewCarRentalServerWithServices("", nil, &mockService, nil)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(carRentalHandler.listUsers)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Status received %d, but expected status %d", status, http.StatusOK)
	}
	data, err := json.Marshal(mockService.users)
	if err != nil {
		t.Fatal(err)
	}
	expected := string(data)
	if rr.Body.String() != expected {
		t.Errorf("body is %s but expected status %s", rr.Body.String(), expected)
	}
}
