package main

import (
	"sync"
	"carrental/model"
	"context"
	"carrental/common"
	"github.com/pborman/uuid"
	"math/rand"
	"time"
	"net/http"
	"bytes"
	"encoding/json"
	"github.com/prometheus/common/log"
)

const(
	WORKER = 20
	URL = "http://localhost:8181/user"
)

func main()  {
	ctx,cancel := context.WithTimeout(context.Background(),30*time.Second)
	defer cancel()
	usersChan := make(chan *model.User)
	var wg sync.WaitGroup
	wg.Add(WORKER+1)
	for i:= 0;i<WORKER;i++  {
		go WorkerTask(i,ctx,&wg,usersChan)
	}
	go LeaderTask(ctx,&wg,usersChan)
	wg.Wait()
	common.Log().Info("Main thread is going to be dead soon...")
}

func WorkerTask(workerId int, ctx context.Context, wg*sync.WaitGroup, userChan chan *model.User)  {
	defer wg.Done()
	for {
		select {
		case user := <-userChan:
			status, err := SendUserSaveReq(user)
			if err != nil {
				common.Log().Error(err)
				continue
			}
			common.Log().Infof("Request is responded with %d status", status)
			case <-ctx.Done():
				common.Log().Infof("Worker: %d has stopped working",workerId)
				return
		}
	}
}

func SendUserSaveReq(user *model.User) (int, error)  {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	data,err := json.Marshal(user)
	if err != nil {
		return 0, err
	}
	byteBuffer := bytes.NewBuffer(data)
	req, err := http.NewRequest(http.MethodPost, URL, byteBuffer)
	if err != nil {
		return 0, err
	}
	req = req.WithContext(ctx)
	req.Header.Set("content-type", "application/json")
	req.Header.Set("Accept","application/json")
	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	return res.StatusCode,nil
}

func LeaderTask(ctx context.Context, wg*sync.WaitGroup, userChan chan *model.User)  {
	defer wg.Done()
	for{
		newUser := model.NewUser(uuid.NewRandom().String(),uuid.NewRandom().String(),uuid.NewRandom().String(),rand.Int())
		log.Infof("Created User %v",newUser)
		select {
		case userChan <-newUser:
		case <-ctx.Done():
			common.Log().Info("Leader has stopped working")
			return
		}
	}
}





