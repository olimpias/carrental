package model

import "fmt"

type Car struct {
	PlateNumber string `json:"plate_number"`
	Model       string `json:"model"`
	Color       string `json:"color"`
}

func NewCar(plateNumber string, model string, color string) *Car {
	return &Car{PlateNumber: plateNumber, Model: model, Color: color}
}

func (c Car) String() string {
	return fmt.Sprintf("Car -> PlateNumber: %v, Model: %v, Color: %v", c.PlateNumber, c.Model, c.Color)
}
