package model

import "fmt"

type User struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	Surname string `json:"surname"`
	Age     int    `json:"age"`
}

func NewUser(id string, name string, surname string, age int) *User {
	return &User{Id: id, Name: name, Surname: surname, Age: age}
}

func (u User) String() string {
	return fmt.Sprintf("User -> ID: %s, Name: %s, Surname: %s, Age %d", u.Id, u.Name, u.Surname, u.Age)
}
