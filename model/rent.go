package model

import "fmt"

type Rent struct {
	Car  *Car   `json:"car"`
	User *User  `json:"user"`
	Id   RentId `json:"-"` //To skip json as a property!
}

type RentId struct {
	id          string
	plateNumber string
}

func NewRentId(id string, plateNumber string) RentId {
	return RentId{id: id, plateNumber: plateNumber}
}

func (rentId RentId) String() string {
	return fmt.Sprintf("RentId -> UserId: %v, PlateNumber: %v", rentId.id, rentId.plateNumber)
}

func NewRent(car *Car, user *User) *Rent {
	return &Rent{Car: car, User: user, Id: NewRentId(user.Id, car.PlateNumber)}
}

func (rent Rent) String() string {
	return fmt.Sprintf("Rent -> %v, %v, %v", rent.Id, rent.User, rent.Car)
}
