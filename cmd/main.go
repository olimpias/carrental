package main

import (
	"carrental/common"
	"carrental/controller"
	"flag"
	"os"

	"os/signal"
	"syscall"
)

var (
	addressFlag      = flag.String("address", ":8181", "Http server ip:port address")
	pprofAddressFlag = flag.String("pAddress", ":6060", "Pprof http server ip:port address")
)

func init() {
	flag.Parse()
}
func main() {
	pprofHandler := controller.NewPprofServer(*pprofAddressFlag)
	controller.InitializePprofRouter(pprofHandler)
	pprofHandler.Serve()
	carRentalHandler := controller.NewCarRentalServer(*addressFlag)
	controller.InitializeRentalRouter(carRentalHandler)
	carRentalHandler.Serve()
	Sigterm()
}

func Sigterm() {
	common.Log().Infof("Click Ctrl+C to stop the service")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	<-c
	os.Exit(0)
}
