package service

import (
	"carrental/common"
	"carrental/model"
	"sync"
)

var (
	CarService ICarService
)

type ICarService interface {
	SaveCar(car *model.Car) error
	FindCar(plateNumber string) (*model.Car, error)
	ListCars() []*model.Car
}

type carServiceImpl struct {
	carMap map[string]*model.Car
	m      sync.RWMutex
}

func init() {
	CarService = &carServiceImpl{carMap: make(map[string]*model.Car)}
}

func (c *carServiceImpl) SaveCar(car *model.Car) error {
	c.m.Lock()
	defer c.m.Unlock()
	if _, ok := c.carMap[car.PlateNumber]; ok {
		common.Log().Errorf("Car already exists with id %v. Err: %v", car.PlateNumber, common.ErrItemAlreadyExists)
		return common.ErrItemAlreadyExists
	}
	c.carMap[car.PlateNumber] = car
	common.Log().Infof("Car is added %v", car)
	return nil
}
func (c *carServiceImpl) ListCars() []*model.Car {
	c.m.RLock()
	defer c.m.RUnlock()
	cars := make([]*model.Car, 0)
	for _, car := range c.carMap {
		cars = append(cars, car)
	}
	return cars
}

func (c *carServiceImpl) FindCar(plateNumber string) (*model.Car, error) {
	c.m.RLock()
	defer c.m.RUnlock()
	car, ok := c.carMap[plateNumber]
	if !ok {
		return nil, common.ErrItemNotFound
	}
	return car, nil
}
