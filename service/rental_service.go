package service

import (
	"carrental/common"
	"carrental/model"
	"sync"
)

var (
	RentalService IRentalService
)

type IRentalService interface {
	RentCar(userId string, plateNumber string) error
	ListRents() []*model.Rent
}

type rentalServiceImpl struct {
	rentMap map[model.RentId]*model.Rent
	m       sync.RWMutex
}

func init() {
	RentalService = &rentalServiceImpl{rentMap: make(map[model.RentId]*model.Rent)}
}

func (rs *rentalServiceImpl) RentCar(userId string, plateNumber string) error {
	rs.m.Lock()
	defer rs.m.Unlock()
	user, err := UserService.FindUser(userId)
	if err != nil {
		return err
	}
	car, err := CarService.FindCar(plateNumber)
	if err != nil {
		return err
	}
	rentId := model.NewRentId(user.Id, car.PlateNumber)
	if _, ok := rs.rentMap[rentId]; ok {
		return common.ErrItemAlreadyExists
	}
	rs.rentMap[rentId] = model.NewRent(car, user)
	common.Log().Infof("Rent has done for %v", rs.rentMap[rentId])
	return nil
}

func (rs *rentalServiceImpl) ListRents() []*model.Rent {
	rents := make([]*model.Rent, 0)
	for _, value := range rs.rentMap {
		rents = append(rents, value)
	}
	return rents
}
