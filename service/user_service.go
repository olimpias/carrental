package service

import (
	"carrental/common"
	"carrental/model"
	"sync"
)

var (
	UserService IUserService
)

type IUserService interface {
	SaveUser(user *model.User) error
	FindUser(id string) (*model.User, error)
	ListUsers() []*model.User
}

type userServiceImpl struct {
	userMap map[string]*model.User
	m       sync.RWMutex
}

func init() {
	UserService = &userServiceImpl{userMap: make(map[string]*model.User)}
}

func (u *userServiceImpl) SaveUser(user *model.User) error {
	u.m.Lock()
	defer u.m.Unlock()
	if _, ok := u.userMap[user.Id]; ok {
		common.Log().Errorf("Car already exists with id %v. Err: %v", user.Id, common.ErrItemAlreadyExists)
		return common.ErrItemAlreadyExists
	}
	u.userMap[user.Id] = user
	common.Log().Infof("User is added %v", user)
	return nil
}
func (u *userServiceImpl) ListUsers() []*model.User {
	u.m.RLock()
	defer u.m.RUnlock()
	users := make([]*model.User, 0)
	for _, user := range u.userMap {
		users = append(users, user)
	}
	return users
}

func (u *userServiceImpl) FindUser(id string) (*model.User, error) {
	u.m.RLock()
	defer u.m.RUnlock()
	user, ok := u.userMap[id]
	if !ok {
		return nil, common.ErrItemNotFound
	}
	return user, nil
}
