### Car
```
curl --header "Content-Type: application/json" --header "Accept: application/json" -X GET http://localhost:8181/car

curl --header "Content-Type: application/json" --header "Accept: application/json" -X POST -d '{"plate_number":"34DK4040","model":"BMW","color":"RED"}' http://localhost:8181/car

```

### User
```
curl --header "Content-Type: application/json" --header "Accept: application/json" -X GET http://localhost:8181/user

curl --header "Content-Type: application/json" --header "Accept: application/json" -X POST -d '{"id":"494949494","name":"cem","surname":"turker","age":14}' http://localhost:8181/user

```

### Rent Car

```
curl --header "Content-Type: application/json" --header "Accept: application/json" -X GET http://localhost:8181/rent

curl --header "Content-Type: application/json" --header "Accept: application/json" -X POST  http://localhost:8181/rent/user/535353535/car/61WC4042
```

### Linter

* Gometalinter

`go get -u github.com/alecthomas/gometalinter`

To download dependencies

`gometalinter -i -u`

Run to see problems...

`gometalinter ./...`

### Pprofing

Install `sudo apt-get install graphviz gv` to display profiles image

`https://medium.com/@hackintoshrao/daily-code-optimization-using-benchmarks-and-profiling-in-golang-gophercon-india-2016-talk-874c8b4dc3c5`
`https://artem.krylysov.com/blog/2017/03/13/profiling-and-optimizing-go-web-applications/`

### UUID

`go get -u github.com/pborman/uuid`

### To Download All Dependencies Under Project

`go get -d ./...`